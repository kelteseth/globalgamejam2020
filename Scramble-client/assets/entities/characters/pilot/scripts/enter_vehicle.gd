extends Node

onready var mp = get_node('/root/Scramble/Multiplayer')

func enter(parent_path_to_enter):
	if get_node("..").is_posessed:
		mp.enter_vehicle(get_node("..").get_path(), parent_path_to_enter)
