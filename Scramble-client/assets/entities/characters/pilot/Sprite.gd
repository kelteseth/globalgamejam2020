extends Sprite

#udate uniforms

var iTime=0.0
var iFrame=0

func _ready():
	pass

func _process(delta):
	iTime+=delta
	iFrame+=1
	self.material.set("shader_param/iTime",iTime)
	self.material.set("shader_param/iFrame",iFrame)

func cov_scb(value):
	self.material.set("shader_param/COVERAGE",float(value)/100)

func absb_scb(value):
	self.material.set("shader_param/ABSORPTION",float(value)/10)

func thick_scb(value):
	self.material.set("shader_param/THICKNESS",value)

func step_scb(value):
	self.material.set("shader_param/STEPS",value)
