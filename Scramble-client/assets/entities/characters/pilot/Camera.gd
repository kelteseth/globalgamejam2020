extends Camera


func _ready():
	var sky_b=get_node_or_null("../../Sky_Clouds") 
	if(sky_b == null):
		return
	var iChannel=sky_b.get_viewport().get_texture()
	self.environment=load("res://default_env.tres") as Environment
	self.environment.background_sky.set_panorama(iChannel)
