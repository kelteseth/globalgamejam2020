extends Area


onready var mp = get_node('/root/Scramble/Multiplayer')


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Shoot_body_entered(body):
    var playerControl = body.get_node_or_null("PlayerControl")
    if playerControl != null:
        playerControl.interactable_shoot = get_node(".")



func _on_Shoot_body_exited(body):
    var playerControl = body.get_node_or_null("PlayerControl")
    if playerControl != null:
        playerControl.interactable_shoot = null

func interact():
    var particles = (get_node("../ShootParticle") as Particles)
    particles.restart()
    
    get_parent().random_rotate()
    
    var raycast = get_node("../RayCast") as RayCast
    raycast.force_raycast_update()
    if raycast.is_colliding():
        var collider = raycast.get_collider()
        var ship_path = collider.get_node("../../../../../..").get_path()
        Global.log("shooting, hit:" + ship_path)
        mp.hit_ship(ship_path)
