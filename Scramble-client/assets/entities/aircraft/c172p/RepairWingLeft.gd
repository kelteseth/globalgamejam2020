extends Area

onready var mp = get_node('/root/Scramble/Multiplayer')

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_RepairWingLeft_body_entered(body):
	var playerControl = body.get_node_or_null("PlayerControl")
	if playerControl != null:
		playerControl.interactable_repair = get_node(".")

	pass # Replace with function body.


func _on_RepairWingLeft_body_exited(body):
	var playerControl = body.get_node_or_null("PlayerControl")
	if playerControl != null:
		playerControl.interactable_repair = null

func interact():
	mp.heal(get_node("../").get_path())
