# Scramble
# Copyright (C) 2018  ScrambleSim and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Spatial

var server_health = 100


onready var mp = get_node('/root/Scramble/Multiplayer')

# Relevant if this instance is a slave/puppet
puppet func _update_position(new_transform):
    if get_tree().get_rpc_sender_id() == 1:
        self.transform = new_transform

func receive_damage():
    mp.hit_ship(get_node("."))

puppet func update_health(new_health):
    self.server_health = new_health

func random_rotate():
    rpc_id(1, "random_rotate")
