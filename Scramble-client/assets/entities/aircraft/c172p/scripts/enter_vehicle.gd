extends Node

func _on_EnterArea_body_entered(body):
	var enter_behaviour = body.get_node_or_null("EnterVehicle")
	if (not enter_behaviour == null):
		enter_behaviour.enter(get_parent().get_path())
		enter_behaviour.queue_free()
