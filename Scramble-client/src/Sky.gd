extends Sprite

#udate uniforms

var iTime=0.0
var iFrame=0

func _ready():
	#self.material.set("shader_param/iChannel0","res://assets/textures/noise.png")
	self.material.set("shader_param/COVERAGE",0.5)
	self.material.set("shader_param/ABSORPTION",1.0)
	self.material.set("shader_param/THICKNESS",25)
	self.material.set("shader_param/STEPS",25)


func _process(delta):
	self.material.set("shader_param/iTime",iTime)
	self.material.set("shader_param/iFrame",iFrame)
	iTime+=delta
	iFrame+=1
