# Scramble
# Copyright (C) 2018  ScrambleSim and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Spatial

var health = 100
var direction = ""

func hit():
    Global.log(str(health))
    if health <= 0:
        return
    health -= 2
    #self.translate(Vector3(0,-10,0))
    #self.rotate_object_local(Vector3(0,1,0), rand_range(-30,30) )
    pass

func heal():
    Global.log(str(health))
    if health >= 100:
        return
    health += 1
    #self.translate(Vector3(0,5,0))
    pass

func _physics_process(delta):
    self.translate(Vector3(-1 * delta, 0, 0))

    if self.direction != null:
        if self.direction == "left":
            self.rotate_object_local(Vector3(0,1,0), 0.01)
        if self.direction == "right":
            self.rotate_object_local(Vector3(0,1,0), -0.01)
      
    
    
    for id in Global.player_ids:
        rpc_id(id, "_update_position", self.transform)
        rpc_id(id, "update_health", self.health)


master func random_rotate():
    self.rotate_object_local(Vector3(0, 1, 0), rand_range(-0.1, 0.1))
