<!-- Please search existing issues for potential duplicates before filing yours:
https://gitlab.com/ScrambleSim/Scramble/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=enhancement
-->

**Story**
<!-- summarize the enhancement concisely-->
As a ROLE I want ENHANCEMENT

**Inspiration**
<!-- Concept art, diagram, resources to get an understanding of the enhancement-->
As seen in this paper, Scramble could implement this new feature.

**Scramble version**
<!-- the last Scramble version you tested-->
v0.0.0

**Acceptance Criteria**
<!-- What is required for users to accept this story? Edit below if applicable-->
* Feature is red
* Feature works in multplayer
* Is translated

<!------------- DO NOT REMOVE LINES BELOW -------------->
**Definition of done**
<!-- What the team requires BEFORE this ticket shall be reviewed -->
* All unit tests pass
* Conforms to the [Coding Conventions](https://gitlab.com/ScrambleSim/Scramble/wikis/Workflows/Coding-Conventions)
* Conforms to the [Gitiquette](https://gitlab.com/ScrambleSim/Scramble/wikis/Workflows/Gitiquette)
* Pull Request is rebased and ready for merge

/label ~enhancement ~discussion
<!------------- DO NOT REMOVE LINES ABOVE -------------->
